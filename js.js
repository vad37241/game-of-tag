//  win array
const winArray = [
    [1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
    [13,14,15,'']
];
//game board
const boardArray = [];

// history click to items
let historyChangeArray = [];

// generate game board items
function getRandomArray() {
    const values = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,''];
    var shuffledArr = values.sort(function(){
        return Math.random() - 0.5;
    });
    return shuffledArr;
}

//show game board
function createBoardItems() {
    let itemCount = 0;
    const arrayValues = getRandomArray();
    for (let i=0; i<4; i++) {
        boardArray[i] = [];
        for (let j = 0; j < 4; j++){
            boardArray[i].push(arrayValues[itemCount]);
            let classNameItem = 'game-board-item' + itemCount;
            $('.game-board').append(`
                <div class="game-board-item"
                  id=${classNameItem}
                  itemI=${i}
                  itemJ=${j}
                  onclick="activeItem(this.attributes['itemI'].value,this.attributes['itemJ'].value,this)">
                    ${arrayValues[itemCount]}
                </div>`)
            itemCount ++;
        }
    }
}

// function click to board items
function activeItem(i,j,elem) {
        const hasActive = $(elem).hasClass('active');
        let countActive = $('.active').length;
        (countActive < 2) ?
            (hasActive ? $(elem).removeClass('active') : $(elem).addClass('active')) :
            ($('.game-board-item').removeClass('active'));
        countActive = $('.active').length;
        countActive == 1 ?
            (historyChangeArray.push(i) && historyChangeArray.push(j)) : ((countActive == 2) ? (historyChangeArray.push(i) && historyChangeArray.push(j) && replacingItems(historyChangeArray)) :
            '');
}

/*
check permission.
historyChange[0] - first item I
historyChange[1] - first item J
historyChange[2] - second item I
historyChange[3] - second item J
 */
function checkReplacingPermission(historyChanges) {
    $('.game-board-item').removeClass('active');
    if ((historyChanges[0] == historyChanges[2]) &&
        ((Number(historyChanges[1])+1 == Number(historyChanges[3])) ||
            Number(historyChanges[1])-1 == Number(historyChanges[3])) && ((boardArray[historyChanges[0]][historyChanges[1]] == '') ||
            (boardArray[historyChanges[2]][historyChanges[3]] == ''))){
        return 1;
    } else if ((historyChanges[1] == historyChanges[3]) &&
        ((Number(historyChanges[0])+1 == Number(historyChanges[2])) ||
            Number(historyChanges[0])-1 == Number(historyChanges[2])) && ((boardArray[historyChanges[0]][historyChanges[1]] == '') ||
            (boardArray[historyChanges[2]][historyChanges[3]] == ''))){
        return 1;
    }else {
        return ;
    }
}

//functions replacing Items
function replacingItems(historyChanges){
    if(checkReplacingPermission(historyChanges)){
      const tempValue = boardArray[historyChanges[0]][historyChanges[1]];
        boardArray[historyChanges[0]][historyChanges[1]] = boardArray[historyChanges[2]][historyChanges[3]];
        $('div[itemI='+ historyChanges[0] + '][itemJ=' + historyChanges[1] + ']').html(boardArray[historyChanges[2]][historyChanges[3]]);
        boardArray[historyChanges[2]][historyChanges[3]] = tempValue;
        $('div[itemI='+ historyChanges[2] + '][itemJ=' + historyChanges[3] + ']').html(tempValue);
    };

    // check win
    if(boardArray.join(',')=== winArray.join(',')){
        alert('you win');
    }
    historyChangeArray.length = 0;
}

createBoardItems();
